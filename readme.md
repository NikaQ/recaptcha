## Introduction

Recaptcha library for laravel

## Installation

#### Install package via composer:

    composer require app3null/recaptcha

## Configuration

#### Publish vendor files using the following artisan command:

    php artisan vendor:publish --provider="App3null\Recaptcha\Providers\RecaptchaServiceProvider"

#### Add your Recaptcha API Keys

Set **RECAPTCHA_SITE_KEY** and **RECAPTCHA_SECRET_KEY** in you .env file

    # .env file
    RECAPTCHA_SITE_KEY=YOUR_API_SITE_KEY
    RECAPTCHA_SECRET_KEY=YOUR_API_SECRET_KEY

## Basic Usage

#### Add recaptcha component into your blade file, where you want recaptcha to be displayed

    <form>
        @csrf
        ...
        <x-app3null-recaptcha />
        ...
    </form>

#### If you want custom classes, add them to recaptcha component like this:

    <form>
        @csrf
        ...
        <x-app3null-recaptcha class="custom classes" />
        ...
    </form>


### On Backend's side add recaptcha rule to g-recaptcha-response parameter:
    use App3null\Recaptcha\Rules\Recaptcha;
    ...
    request()->validate([
        ...
        'g-recaptcha-response'=>[new Recaptcha, 'required'], // other rules go her
    ]);
    ...
# Localization

### To change error messages you can navigate to resources/lang/vendor/recaptcha 
### and change recaptcha invalid message from there

## License

The library is an open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
