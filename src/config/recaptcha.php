<?php

return [
    'validate_url' => 'https://www.google.com/recaptcha/api/siteverify',
    'secret_key' => env('RECAPTCHA_SECRET_KEY', ''),
    'site_key' => env('RECAPTCHA_SITE_KEY', '')
];
