<?php

namespace App3null\Recaptcha\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Http;

class Recaptcha implements Rule
{
    public function passes($attribute, $value): bool
    {
        return $this->isValid($value);
    }

    private function isValid($recaptcha): bool
    {
        if (config('app.debug')) {
            return true;
        }
        $response = Http::post(config_path('recaptcha.validate_url'), [
            'secret' => config('recaptcha.secret_key'),
            'response' => $recaptcha
        ]);
        return $response->json()['success'];
    }

    public function message(): string
    {
        return __("recaptcha::recaptcha.invalid");
    }
}
