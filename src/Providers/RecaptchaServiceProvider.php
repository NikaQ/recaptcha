<?php

namespace App3null\Recaptcha\Providers;

use App3null\Recaptcha\View\Components\Recaptcha;
use Illuminate\Support\ServiceProvider;


class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'recaptcha');
        $this->publishes([
            __DIR__ . '/../config/recaptcha.php' => config_path('recaptcha.php'),
        ]);

        $this->publishes([
            __DIR__ . '/../resources/lang' => resource_path('lang/vendor/recaptcha'),
        ]);
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'app3null');
        $this->loadViewComponentsAs('app3null', [
            Recaptcha::class,
        ]);
    }
}
