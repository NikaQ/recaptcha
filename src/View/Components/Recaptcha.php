<?php

namespace App3null\Recaptcha\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Recaptcha extends Component
{
    public $siteKey;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->siteKey = config('recaptcha.site_key');
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('app3null::components.recaptcha');
    }
}
