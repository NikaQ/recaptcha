<div data-sitekey="{{$siteKey}}" {{ $attributes->merge(['class' => 'g-recaptcha']) }}></div>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
